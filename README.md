# Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
+[#cyan] Programación declarativa
++ paradigma de programación
+++ Surge como reacción a los problemas de la programación clásica (programacion imperativa)
++++ C, pascal
+++++ El programador da exceso de detalles sobre los cálculos a realizar
+++++ Instrumento
++++++ Asignación
+++++++ Modifica el estado de la memoria del ordenador en detalle
***_ propósito
++++ liberarse de las asignaciones
++++ liberarse de detallar especificamente cual es el control de la gestion de memoria en el ordenador
++++ utilizar otros recursos que permitan especificar los programas a un nivel más alto
*****_ ventajas
++++++ programas más cortos
++++++ programas más fáciles de realizar
++++++ programas más fáciles de depurar
***_ Variantes
++++ programación funcional
+++++ Recurre al lenguaje de los matemáticos
++++++ Se hacen mediante funciones matemáticas
++++++ es muy útil para especificar programas sin hacer gestion detallada de la memoria
*****_ Ventajas
++++++ Funciones de orden superior
+++++++ una vez que se tiene como objetos principales tu lenguaje las funciones, se puede definir funciones que actúen sobre funciones
++++++ evaluación perezosa
******_ Consiste en
+++++++ solamente se evalúan las funciones, es decir, solo se computa aquello que es estrictamente necesario para hacer cálculos posteriores
********_ Ejemplo
+++++++++ Serie de fibonacci
*****_ Lenguaje
++++++ LISP
++++ programación lógica
*****_ Recurre a
++++++ Lógica de predicados de primer orden
+++++++ Son relaciones entre objetos que estamos definiendo
+++++++ no establece un orden entre argumentos de entrada y datos de salida 
*****_ Como funciona?
++++++ El intérprete dado un conjunto de relaciones y predicados opera mediante algoritmos de resolución
+++++++ Intenta demostrar predicados dentro de este sistema
*****_ Lenguaje
++++++ PROLOG
*****_ se basa en
++++++ demostración de la lógica
*****_ utiliza
++++++ Axiomas y reglas de inferencia
***_ Desventajas
++++ Se programa a base de una idea abstracta
++++ Puede estar demasiado alejado para que la eficiencia resultante de los programas sea igual que cuando se está directamente diciendo a la máquina lo que tiene que hacer
@endmindmap
```
# Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
+ Paradigma de programación
**_ que es?
+++ Son el modelo de computación en la que los lenguajes dotan de semántica a los programas
**_ se basa en
+++ modelo de computación de Von Neumann
***_ Propuso
++++ Los programas debían almacenarse en la misma máquina antes de ser ejecutados
+++++ las instrucciones se ejecutaban de manera secuencial
**_ variantes
+++ Programación orientado a objetos
+++[#cyan] Programación lógica
****_ se forma
+++++ mediante un conjunto de sentencias que definen lo que es verdad y lo que es conocido para un programa con respecto de un problema
****_ utiliza
+++++ Reglas de inferencia
****_ base
+++++ Lamda cálculo
******_ desarrolló
+++++++ la lógica de la combinatoria
**_ que es un programa?
+++ Programación imperativa
++++ es una colección de datos, una serie de instrucciones que se ejecutan en el orden adecuado
+++[#cyan] programación funcional
++++ Funciones matemáticas
++++ No hay bucles
****_ Existe 
+++++ la recursión
******_ es
+++++++ una función que se llama a si misma
+++++ las constantes
****_ depende de
+++++ Los parámetros de entrada
****_ ventajas
+++++ En entorno multiprocesador
++++++ se puede paralelizar y luego combinarlos para producir un resultado
+++++ puede devolver funciones
@endmindmap
```
